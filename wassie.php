<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/stywassie.css">
    <title>Wassie</title>
</head>
<body>
<div class="container">
		<div class="messages">
			<ul id="messagesList">
				<!-- messages will be added here dynamically -->
			</ul>
		</div>
		<div class="input">
			<form id="newMessageForm">
				<input type="text" id="phoneNumber" placeholder="Phone number" required>
				<input type="text" id="username" placeholder="Username" required>
				<textarea id="message" placeholder="Message" required></textarea>
				<button type="submit">Enviar</button>
			</form>
		</div>
	</div>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
	<script src="js/appswassie.js"></script>
</body>
</html>