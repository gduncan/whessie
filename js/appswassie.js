$(document).ready(function() {
    // Load messages on page load
    /* $.ajax({
        url: "https://mamba.dev/listmensajes",
        type: "GET",
        dataType: "json",
        success: function(data) {
            for (var i = 0; i < data.length; i++) {
                addMessage(data[i]);
            }
        },
        error: function(jqXHR, textStatus, errorThrown) {
            console.log("Error loading messages: " + errorThrown);
        }
    }); */

    // Send new message on form submit
    $("#newMessageForm").submit(function(event) {
        event.preventDefault();
        var phoneNumber = $("#phoneNumber").val();
        var username = $("#username").val();
        var message = $("#message").val();
        /* $.ajax({
            url: "https://mamba.dev/newmensaje",
            type: "POST",
            data: {phoneNumber: phoneNumber, username: username, message: message},
            dataType: "json",
            success: function(data) {
                addMessage(data);
                $("#phoneNumber").val("");
                $("#username").val("");
                $("#message").val("");
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log("Error sending message: " + errorThrown);
            }
        }); */
        addMessage(
            {
                username: phoneNumber,
                phoneNumber: username,
                message: message
            }
        );
    });

    // Add message to the messages list
    function addMessage(message) {
        var html = "<li><span class='user'>" + message.username + "</span> <span class='phone'>" + message.phoneNumber + "</span> <span class='message'>" + message.message + "</span></li>";
        $("#messagesList").append(html);
    }
});
