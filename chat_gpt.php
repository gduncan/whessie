<?php
require_once("env.php");
require_once("appServerProto/gptMachine.php");

$gpt = new gptMachine();
$question = isset($_GET['question']) ? $_GET['question'] : "";

?>

<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>ChatGpt php prueba</title>
    <link rel="stylesheet" href="css/styles.css">
</head>

<body>
    <div class="container">
        <div class="lorem">
            <form action="/chat_gpt.php" method="GET">
                <label for="consulta">Consulta:</label>
                <input type="text" name="question" value="<?php echo $question; ?>" id="consulta" required>
                <button type="submit">Enviar</button>
            </form>
            <div class="texto-ans-chat">

                <pre>
                <?php

                if ($question !== "") {
                    $respuesta = $gpt->keepQueryToChat($question);
                    echo $respuesta;
                }

                ?>
                </pre>

            </div>
        </div>

        <div class="historicas">
            <?php
            $chats = $gpt->getHistoricos();
            ?>
            <table class="blueTable">
                <thead>
                    <th>
                        <tr>
                            <th>Consulta</th>
                            <th>Respuesta</th>
                        </tr>
                    </th>
                </thead>
                <tbody>
                    <?php
                    if ($chats) {
                        foreach ($chats as $clave => $valor) {
                    ?>
                            <tr>
                                <td><?php echo $valor['prompt']; ?></td>
                                <td><?php echo $valor['answer']; ?></td>
                            </tr>
                    <?php
                        }
                    }
                    ?>
                </tbody>
            </table>
        </div>

    </div>
    <script src="js/app.js"></script>
</body>

</html>