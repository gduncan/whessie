<?php
require_once('chatGptConfig.php');
require_once('queryMachineModel.php');

class gptMachine extends chatGptConfig
{
    private $queryMachineModel;

    public function __construct()
    {
        $this->queryMachineModel = new queryMachineModel();
    }

    public function keepQueryToChat($prompt){
        $prompTokeep = $prompt;
        $ansToKeep = $this->queryPromptToChat($prompt);
        $this->queryMachineModel->insertChatGPTQuery($prompTokeep,$ansToKeep);
        return $ansToKeep;
    }

    public function getHistoricos(){
        return $this->queryMachineModel->allChatsList();
    }

    public function queryPromptToChat($prompt)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->gptcompletionsURL);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Content-Type: application/json',
            'Authorization: Bearer ' . $this->apiKey,
        ]);
        //INICIAMOS EL JSON QUE SE ENVIARA A META
        curl_setopt($ch, CURLOPT_POSTFIELDS, "{
            \"model\": \"text-davinci-003\",
            \"prompt\": \"" . $prompt . "\",
            \"max_tokens\": 4000,
            \"temperature\": 1.0
        }");

        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        //OBTENEMOS EL JSON COMPLETO CON LA RESPUESTA
        $response = curl_exec($ch);
        curl_close($ch);
        $decoded_json = json_decode($response, false);
        //var_dump($decoded_json);
        //RETORNAMOS LA RESPUESTA QUE EXTRAEMOS DEL JSON
        return  $decoded_json->choices[0]->text;
    }
}

?>
