<?php
require_once('whassieMachineConfig,php');
require_once('queryMachineModel.php');
require_once("appServerProto/gptMachine.php");

class whassieMachine extends whassieMachineConfig
{
    private $queryMachineModel;

    public function __construct()
    {
        $this->queryMachineModel = new queryMachineModel();
    }

    public function receiveMessage()
    {
        /*
        * VERIFICACION DEL WEBHOOK
        */
        //TOQUEN QUE QUERRAMOS PONER 
        $token = $this->webhook_token_spacios;
        //RETO QUE RECIBIREMOS DE FACEBOOK
        $palabraReto = $_GET['hub_challenge'];
        //TOQUEN DE VERIFICACION QUE RECIBIREMOS DE FACEBOOK
        $tokenVerificacion = $_GET['hub_verify_token'];
        //SI EL TOKEN QUE GENERAMOS ES EL MISMO QUE NOS ENVIA FACEBOOK RETORNAMOS EL RETO PARA VALIDAR QUE SOMOS NOSOTROS
        if ($token === $tokenVerificacion) {
            echo $palabraReto;
            exit;
        }
        /*
        * RECEPCION DE MENSAJES
        */
        //LEEMOS LOS DATOS ENVIADOS POR WHATSAPP
        $respuesta = file_get_contents("php://input");
        //CONVERTIMOS EL JSON EN ARRAY DE PHP
        $respuesta = json_decode($respuesta, true);
        //EXTRAEMOS EL TELEFONO DEL ARRAY
        $phone = $respuesta['entry'][0]['changes'][0]['value']['messages'][0]['from'];
        $mensaje = $respuesta['entry'][0]['changes'][0]['value']['messages'][0]['text']['body'];
        
        if($phone && $mensaje){
            $this->queryMachineModel->insertMessageWhassie($phone,'56944389177',$mensaje);
            if ($this->auto_pilot_chatgpt) {
                $answer = str_replace("\n","",$this->askChatGpt($mensaje));
                $this->sendMessage($phone, $answer);
            }
        }
    }
    
    private function askChatGpt($prompt){
        $prompt = $prompt." resumido en menos de 150 palabras";
        $gpt = new gptMachine();
        $respuesta = $gpt->keepQueryToChat($prompt);
        return $respuesta;
    }

    public function sendMessage($phone, $message)
    {
        //TOKEN QUE NOS DA FACEBOOK
        $token = $this->perm_token;
        //URL A DONDE SE MANDARA EL MENSAJE
        $url = $this->url_send_message;

        //CONFIGURACION DEL MENSAJE
        $mensaje = ''
        . '{'
        . '"messaging_product": "whatsapp", '
        . '"recipient_type": "individual",'
        . '"to": "'.$phone.'", '
        . '"type": "text", '
        . '"text": '
        . '{'
        . '     "body": "'.$message.'",'
        . '     "preview_url": true'
        . '} '
        . '}';
        //DECLARAMOS LAS CABECERAS
        $header = array("Authorization: Bearer " . $token, "Content-Type: application/json",);
        //INICIAMOS EL CURL
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $mensaje);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        //OBTENEMOS LA RESPUESTA DEL ENVIO DE INFORMACION
        $response = json_decode(curl_exec($curl), true);
        //IMPRIMIMOS LA RESPUESTA 
        //print_r($response);
        //OBTENEMOS EL CODIGO DE LA RESPUESTA
        $status_code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        //CERRAMOS EL CURL
        curl_close($curl);
        if($phone && $message){
            $this->queryMachineModel->insertMessageWhassie('56944389177', $phone, $message);
        }
        //return $response;
    }
}
