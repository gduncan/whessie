<?php 
require_once('queryMachineConnConfig.php');

class queryMachineConn extends connConfig{
    private $conn;
    
    public function __construct()
    {
        $this->conn;
    }

    public function testConnection(){
        $this->db_stablish_connection();
        if(!$this->conn){
            die("Connection failed: " . mysqli_connect_error());
        } else{
            echo 'conexión exitosa';
        }
        $this->db_close_connection(); 
    }

    public function runInsertQuery($query){
        $this->db_stablish_connection();
        if ($result = $this->conn->query($query)) {
            return $this->conn->insert_id;
        }else{
            return -1;
        }
        $this->db_close_connection();
    }

    public function runQuery($query){
        $this->db_stablish_connection();
        $ans = null;
        if ($result = $this->conn->query($query)) {    
            if ($result->num_rows > 0) {
                $ans = [];  
                while($row = mysqli_fetch_assoc($result)) {
                    $ans[] = $row;
                }
            }            
        }

        return $ans;
        $this->db_close_connection();
    }

    private function db_close_connection(){
        $this->conn->close();
    }

    private function db_stablish_connection(){
        $this->conn = new mysqli(
            $this->server, 
            $this->user, 
            $this->password, 
            $this->database
        );
    }


}
?>