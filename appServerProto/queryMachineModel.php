<?php
require_once('queryMachine.php');

class queryMachineModel extends queryMachine
{
    private $queryMachine;

    public function __construct()
    {
        $this->queryMachine = new queryMachine();
    }

    public function insertMessageWhassie($phone, $to_phone, $message){
        $query = "INSERT INTO mensajes_en_bruto ".
                 "(telefono, receptor, message) VALUES (".
                 "'".$phone."','".$to_phone."','".$message."'".
                 ")";

        $this->queryMachine->runInsertQuery($query);
    }

    public function allWassieMessagesList(){
        $query = "SELECT telefono, receptor, message FROM mensajes_en_bruto";
        $result = $this->queryMachine->runQuery($query);
        
        return $result;
    }
    
    public function insertChatGPTQuery($prompt, $answer){
        $query = "INSERT INTO pompt_querys_en_bruto ".
                 "(prompt, answer) VALUES (".
                    "'".$prompt."','".$answer."'".
                 ")";

        $this->queryMachine->runInsertQuery($query);
    }

    public function allChatsList(){
        $query = "SELECT prompt, answer FROM pompt_querys_en_bruto";
        $result = $this->queryMachine->runQuery($query);
        
        return $result;
    }
}

?>